from selenium import webdriver

def test_task():
    driver = webdriver.Firefox(executable_path=r'.\resources\geckodriver.exe')
    driver.get("https://www.gosuslugi.ru")
    driver.find_element_by_id("_epgu_el1").click()
    driver.find_element_by_id("_epgu_el1").send_keys("загран")
    driver.implicitly_wait(1)
    driver.find_element_by_xpath("//span[contains(text(),'загранпаспорт нового поколения 18 лет')]").click()
    driver.find_elements_by_css_selector("em.ng-binding")[2].click()
    elements = driver.find_elements_by_xpath("//*[contains(text(),'Оформление и выдача заграничного паспорта нового поколения (сроком действия десять лет)')]")
    assert len(elements) == 1, "В результатах поиска нет «Оформление и выдача заграничного паспорта нового поколения (сроком действия десять лет)»,"
    elements[0].click()
    driver.find_element_by_xpath("//*[contains(text(),'Вернуться')]").click()
    assert driver.find_element_by_xpath("//h2").text == "Каталог госуслуг", "После нажатия кнопки \"Вернуться\" НЕ открылось окно каталога гос услуг"
